package ru.innopolis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 */
public class Client {

    private volatile boolean stopped;
    private int port;

    public Client(int port) {
        this.port = port;
    }

    public void start() {
        try (Socket socket = new Socket("localhost", port);
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(socket.getOutputStream()))) {

            new Thread(() -> {
                try (BufferedReader reader = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()))) {
                    String message = null;
                    while (!stopped && (message = reader.readLine()) != null) {
                        System.out.println(message);
                    }
                    System.out.println("Client stopped...");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            Scanner scanner = new Scanner(System.in);
            String line = null;
            while (!"-q".equals(line = scanner.nextLine())) {
                writer.write(line);
                writer.newLine();
                writer.flush();
            }

            stopped = true;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
       Client client = new Client(5555);
       client.start();
    }
}
