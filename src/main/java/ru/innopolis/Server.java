package ru.innopolis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 */
public class Server {

    private volatile boolean stopped;
    private ServerSocket serverSocket;

    private List<BufferedWriter> broadcastWriters;

    public Server(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
        this.broadcastWriters = new ArrayList<>();
    }

    public void start() {
        Thread socketsListenerThread = new Thread(() -> {

            while (!stopped) {
                try {
                    Socket socket = serverSocket.accept();
                    new Thread(() -> {
                        registerAndListen(socket);
                    }).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        socketsListenerThread.start();

        Scanner scanner = new Scanner(System.in);
        String line = null;
        while (!"-q".equals(line = scanner.nextLine())) {
            try {
                broadcastMessage(line);
                System.out.println(line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        stopped = true;
    }

    private void registerAndListen(Socket socket) {

        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));

             BufferedWriter writer = new BufferedWriter(
                     new OutputStreamWriter(socket.getOutputStream()))) {

            broadcastWriters.add(writer);

            String message = null;
            while ((message = bufferedReader.readLine()) != null) {
                broadcastMessage(message);
                System.out.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void broadcastMessage(String message) throws IOException {
        for (BufferedWriter writer : broadcastWriters) {
            writer.write(message);
            writer.newLine();
            writer.flush();
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server(new ServerSocket(5555));
        server.start();
    }
}
